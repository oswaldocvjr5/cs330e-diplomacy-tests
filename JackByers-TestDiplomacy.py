from io import StringIO

from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve, diplomacy_print

class TestDiplomacy (TestCase) :
    def test_solve_1(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")

    def test_solve_2(self):
        r = StringIO("A Paris Hold B London Hold C Georgia Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Paris\nB London\nC Georgia\n")

    def test_solve_3(self):
        r = StringIO('A Madrid Move Barcelona B Barcelona Move Madrid C London Move Austin D Austin Move London')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Madrid\nC Austin\nD London\n")

    def test_solve_4(self):
        r = StringIO("A Houston Support B B Austin Support D C Forthworth move Dallas D Dallas Hold E Galveston Support D")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(),'A Houston\nB Austin\nC [dead]\nD Dallas\nE Galveston\n')

    def test_solve_5(self):
        r = StringIO('A Seattle Support B B Chicago Move Columbus C Columbus Move Chicago')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Seattle\nB Columbus\nC Chicago\n")

    def test_solve_6(self):
        r = StringIO('A Lewisville move Dallas B Dallas Hold')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_print_1(self):
        end = []
        end.append(["A", "Madrid"])
        end.append(["B", "Kyiv"])
        end.append(["C", "[dead]"])
        w =StringIO()
        diplomacy_print(w, end)
        self.assertEqual(w.getvalue(), "A Madrid\nB Kyiv\nC [dead]\n")

    def test_print_2(self):
        end = []
        end.append(['A', 'Chile'])
        end.append(['B', 'Argentina'])
        end.append(['C', 'Nicaragua'])
        w = StringIO()
        diplomacy_print(w, end)
        self.assertEqual(w.getvalue(), 'A Chile\nB Argentina\nC Nicaragua\n')

    def test_print_3(self):
        end = []
        end.append(['A', '[dead]'])
        end.append(['B', '[dead]'])
        end.append(['C', 'Springfield'])
        w = StringIO()
        diplomacy_print(w, end)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC Springfield\n')


    def test_read_1(self):
        lines = "A Madrid Move Barcelona\nB Barcelona Move Madrid\nC London Move Austin"
        self.assertEqual(str(diplomacy_read(lines)), str("[['A', 'Madrid', 'Move', 'Barcelona'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Austin']]"))

    def test_read_2(self):
        lines = "A Amarillo Hold\nB Bedford Support C\nC Houston Hold"
        self.assertEqual(str(diplomacy_read(lines)), str("[['A', 'Amarillo', 'Hold'], ['B', 'Bedford', 'Support', 'C'], ['C', 'Houston', 'Hold']]"))

    def test_read_3(self):
        lines = "A Central Move Hurst\nB Hurst Hold"
        self.assertEqual(str(diplomacy_read(lines)), str("[['A', 'Central', 'Move', 'Hurst'], ['B', 'Hurst', 'Hold']]"))

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacyout
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
Diplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
