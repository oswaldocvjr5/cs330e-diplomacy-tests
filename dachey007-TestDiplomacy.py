#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -------------
# TestDiplomacy
# -------------

class TestDiplomacy(TestCase):
    # ----
    # read
    # ----
    def test_read_1(self):
        s = StringIO("A Madrid Hold\n B Barcelona Move Madrid\n")
        data = diplomacy_read(s)
        self.assertEqual(data, {'A': {'loc': 'Madrid', 'action': 'Hold'},
            'B': {'loc': 'Barcelona', 'action': 'Move', 'moveto': 'Madrid'}})
     
    def test_read_2(self):
        s = StringIO("A Madrid Hold\n B Barcelona Support A\n")
        data = diplomacy_read(s)
        self.assertEqual(data, {'A': {'loc': 'Madrid', 'action': 'Hold'},
            'B': {'loc': 'Barcelona', 'action': 'Support', 'support': 'A'}})
    
    def test_read_3(self):
        s = StringIO("A Madrid Hold\n")
        data = diplomacy_read(s)
        self.assertEqual(data, {'A': {'loc': 'Madrid', 'action': 'Hold'}})
    
    # ----
    # eval
    # ----
    
    def test_eval_1(self):
        d = {'A': {'loc': 'Madrid', 'action': 'Hold'},
             'B': {'loc': 'Barcelona', 'action': 'Hold'}}
        r = diplomacy_eval(d)
        self.assertEqual(r,{"A": {'loc':"Madrid"},"B": {'loc':"Barcelona"}})
    
    def test_eval_2(self):
        d = {'A': {'loc': 'Madrid', 'action': 'Hold'},
             'B': {'loc': 'Barcelona', 'action': 'Move', 'moveto': 'Madrid'},
             'C': {'loc': 'Charlotte', 'action': 'Move', 'moveto': 'Madrid'}}
        r = diplomacy_eval(d)
        self.assertEqual(r, {"A": {'loc':"[dead]"}, "B": {'loc':"[dead]"}, "C": {'loc':"[dead]"}})
    
    def test_eval_3(self):
        d = {'A': {'loc': 'Madrid', 'action': 'Hold'}, 
             'B': {'loc': 'Barcelona', 'action': 'Support', 'support': 'A'}}
        r = diplomacy_eval(d)
        self.assertEqual(r, {"A": {'loc':"Madrid"}, "B": {'loc':"Barcelona"}})
    
    # -----
    # print
    # -----
    
    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, {"A": {'loc': 'Madrid'}})
        self.assertEqual(w.getvalue(), "A Madrid\n")
    
    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, {"A": {'loc': 'Madrid'}, "B": {'loc': '[dead]'}})
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\n")
    
    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, {"A": {'loc': 'Madrid'}, "B": {'loc': 'Barcelona'}})
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\n")
    
    # -----
    # solve
    # -----
    
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")
    
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
    
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\n")
    
if __name__ == "__main__": #pragma: no cover
    main()
