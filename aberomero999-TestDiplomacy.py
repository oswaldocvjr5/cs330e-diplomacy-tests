#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/Testdiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from diplomacy import diplomacy_read, diplomacy_solve, diplomacy_run, clean_slate, exclusive_max, best_army, kill_army

# global dict_cities1
# global dict_cities2
# global dict_armies
# global support
# global supported

# -----------
# TestCollatz
# -----------


class Testdiplomacy (TestCase):
    # ----
    # read
    # ----

    # diplomacy_read

    def test_diplomacy_read_1(self):
        s = "A Madrid Hold\n"
        i, j, k, m, n = diplomacy_read(s)
        self.assertEqual(i,  {'Madrid' : [1]})
        self.assertEqual(j, {'Madrid' : ['A']})
        self.assertEqual(k, {'A': 'Madrid'})
        self.assertEqual(m, [])
        self.assertEqual(n, [])

    def test_diplomacy_read_2(self):
        s = "B Barcelona Move Madrid\n"
        i, j, k, m, n = diplomacy_read(s)
        self.assertEqual(i,  {'Madrid' : [1, 1]})
        self.assertEqual(j, {'Madrid' : ['A', 'B']})
        self.assertEqual(k, {'A': 'Madrid', 'B': 'Madrid'})
        self.assertEqual(m, [])
        self.assertEqual(n, [])

    def test_diplomacy_read_3(self):
        s = "C London Support B\n"
        i, j, k, m, n = diplomacy_read(s)
        self.assertEqual(i,  {'Madrid' : [1, 1], 'London': [1]})
        self.assertEqual(j, {'Madrid' : ['A', 'B'], 'London': ['C']})
        self.assertEqual(k, {'A': 'Madrid', 'B': 'Madrid', 'C': 'London'})
        self.assertEqual(m, ['C'])
        self.assertEqual(n, ['B'])

     #diplomacy_run   

    def test_diplomacy_run_1(self):
        # s = "D Austin Move London\n"
        # i, j, k, m, n = diplomacy_read(s)
        w = StringIO()
        diplomacy_run(w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n\n")

    def test_diplomacy_run_2(self):
        # s = "D Austin Move London\n"
        # i, j, k, m, n = diplomacy_read(s)
        w = StringIO()
        diplomacy_run(w)
        self.assertEqual(w.getvalue(), "\n")

    def test_diplomacy_run_3(self):
        
        s = ["A Madrid Hold\n", "B Barcelona Move Madrid\n", "C London Move Madrid\n", "D Paris Support B\n", "E Austin Support A\n"]
        for strg in s:
            diplomacy_read(strg)

        w = StringIO()
        diplomacy_run(w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n\n")

    # diplomacy_solve

    def test_diplomacy_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n\n")

    def test_diplomacy_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n\n")
    
    def test_diplomacy_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n\n")

    # clean_slate

    def test_clean_slate_1(self):
        i, j, k, m, n = clean_slate()
        self.assertEqual(i, {})
        self.assertEqual(j, {})
        self.assertEqual(k, {})
        self.assertEqual(m, [])
        self.assertEqual(n, [])

    def test_clean_slate_2(self):
        s = "A Madrid Hold\n"
        diplomacy_read(s)
        i, j, k, m, n = clean_slate()
        self.assertEqual(i, {})
        self.assertEqual(j, {})
        self.assertEqual(k, {})
        self.assertEqual(m, [])
        self.assertEqual(n, [])

    def test_clean_slate_3(self):
        s = ["A Madrid Hold\n", "B Barcelona Move Madrid\n", "C London Move Madrid\n", "D Paris Support B\n", "E Austin Support A\n"]
        for strg in s:
            diplomacy_read(strg)
        i, j, k, m, n = clean_slate()
        self.assertEqual(i, {})
        self.assertEqual(j, {})
        self.assertEqual(k, {})
        self.assertEqual(m, [])
        self.assertEqual(n, [])

    # exclusive_max

    def test_exclusive_max_1(self):
        tup = (1,3,1)
        self.assertEqual(exclusive_max(tup), True)

    def test_exclusive_max_2(self):
        tup = (1,3,3)
        self.assertEqual(exclusive_max(tup), False)

    def test_exclusive_max_3(self):
        tup = (1,)
        self.assertEqual(exclusive_max(tup), True)

    # best_army

    def test_best_army_1(self):
        tup1, tup2 = [1,3,1], ['A', 'B', 'C']
        self.assertEqual(best_army(tup1,tup2), "B")

    def test_best_army_2(self):
        tup1, tup2 = [1,1,1], ['A', 'B', 'C']
        self.assertEqual(best_army(tup1,tup2), "A")

    def test_best_army_3(self):
        tup1, tup2 = [1,3,4,], ['A', 'B', 'C']
        self.assertEqual(best_army(tup1,tup2), "C")

    # kill_army

    def test_kill_army_1(self):
        self.assertEqual(kill_army('H'), {'H': '[dead]'})

    def test_kill_army_2(self):
        self.assertEqual(kill_army('J'), {'H': '[dead]', 'J': '[dead]'})

    def test_kill_army_3(self):
        self.assertEqual(kill_army('K'), {'H': '[dead]', 'J': '[dead]', 'K': '[dead]'})


# ----
# main
# ----


if __name__ == "__main__":
    main()

