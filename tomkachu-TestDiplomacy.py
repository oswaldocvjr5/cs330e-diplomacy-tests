from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

class TestDiplomacy (TestCase):

    def test_solve_1(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_2(self):
        r = StringIO(
            "A Madrid Support C\nB Barcelona Move Madrid \nC Austin Support A\nD Dallas Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_3(self):
        r = StringIO(
            "A Madrid Move Barcelona\nB Barcelona Hold\nC London Move Barcelona\nD Chicago Support C\nE Austin Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Chicago\nE Austin\n")
    
    def test_solve4(self):
        r = StringIO("A Madrid Move London\nB Barcelona Move London\nC London Hold\nD Chicago Support A\nE Austin Support B\nF SanFrancisco Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB London\nC [dead]\nD Chicago\nE Austin\nF SanFrancisco\n")

    def test_solve5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Chicago Support B\nE Austin Support A\nF SanFrancisco Support B\n")
        r = StringIO("A Madrid Move Austin\nB Barcelona Move Austin\nC London Support A\nD Chicago Support E\nE Austin Hold\nF SanFrancisco Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB [dead]\nC London\nD Chicago\nE [dead]\nF SanFrancisco\n")

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
"""