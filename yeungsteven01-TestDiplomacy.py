#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve, diplomacy_print, diplomacy_fight, diplomacy_supportWho, diplomacy_supportCount, diplomacy_endLoc, diplomacy_eval

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    
    # -----
    # eval 
    # -----
    
    def test_solve_1(self):
        r = StringIO(
            "A Austin Move Houston\nB Houston Move Austin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Houston\nB Austin\n")

    def test_solve_2(self):
        r = StringIO(
            "A Madrid Support B\nB Austin Hold\nC Houston Move Austin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Austin\nC [dead]\n")
            
    def test_solve_3(self):
        r = StringIO(
            "A Austin Support B\nB Boston Support C\nC Charlotte Hold\nD Dallas Move Boston\nE Edmonton Move Charlotte\nF FortWorth Support E")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB Boston\nC [dead]\nD [dead]\nE Charlotte\nF FortWorth\n")
    # -----
    # solve 
    # -----

    def test_eval_1(self):
        r = {'Hold': ['B', 'Barcelona'], 'Support': ['C', 'Austin', 'A', 'T', 'Miami', 'B', 'F', 'Seattle', 'B'], 'Move': ['A', 'Madrid', 'Barcelona']}
        w = diplomacy_eval(r)
        self.assertEqual(
            w, {'A': '[dead]', 'B': 'Barcelona', 'C': 'Austin', 'F': 'Seattle', 'T': 'Miami'})

    def test_eval_2(self):
        r = {'Hold': ['A', 'Madrid'], 'Support': ['D', 'Paris', 'B'], 'Move': ['B', 'Barcelona', 'Madrid', 'C', 'London', 'Madrid']}
        w = diplomacy_eval(r)
        self.assertEqual(
            w, {'A': '[dead]', 'B': 'Madrid', 'C': '[dead]', 'D': 'Paris'})

    def test_eval_3(self):
        r = {'Hold': ['Z', 'Austin','A','Houston'], 'Support': ['D', 'Dallas', 'Z'], 'Move': ['R', 'Boston', 'Austin']}
        w = diplomacy_eval(r)
        self.assertEqual(
            w, {'A': 'Houston', 'D': 'Dallas', 'R': '[dead]', 'Z': 'Austin'})
    
    def test_eval_4(self):
        r = {'Hold': ['A', 'Madrid'], 'Support': ['D', 'Paris', 'B'], 'Move': ['B', 'Barcelona', 'Madrid', 'C', 'London', 'Paris']}
        w = diplomacy_eval(r)
        self.assertEqual(
            w, {'A': '[dead]', 'B': '[dead]', 'C': '[dead]', 'D': '[dead]'})
    # -----
    # read
    # -----

    def test_read_1(self):
        r = 'A Madrid Move Barcelona\nB Barcelona Hold\nC Austin Support A\nT Miami Support B\nF Seattle Support B'
        w = diplomacy_read(r)
        self.assertEqual(
            w, {'Hold': ['B', 'Barcelona'], 'Support': ['C', 'Austin', 'A', 'T', 'Miami', 'B', 'F', 'Seattle', 'B'], 'Move': ['A', 'Madrid', 'Barcelona']})

    def test_read_2(self):
        r = 'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B'
        w = diplomacy_read(r)
        self.assertEqual(
            w, {'Hold': ['A', 'Madrid'], 'Support': ['D', 'Paris', 'B'], 'Move': ['B', 'Barcelona', 'Madrid', 'C', 'London', 'Madrid']})

    def test_read_3(self):
        r = 'Z Austin Hold\nA Houston Hold\nD Dallas Support Z\nR Boston Move Austin'
        w = diplomacy_read(r)
        self.assertEqual(
            w, {'Hold': ['Z', 'Austin','A','Houston'], 'Support': ['D', 'Dallas', 'Z'], 'Move': ['R', 'Boston', 'Austin']})


    # -----
    # print
    # -----
    
    def test_print_1(self):
        r = {'A': '[dead]', 'B': 'Barcelona', 'C': 'Austin', 'F': 'Seattle', 'T': 'Miami'}
        w = StringIO()
        diplomacy_print(w,r)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Barcelona\nC Austin\nF Seattle\nT Miami\n")
            
    def test_print_2(self):
        r = {'A': '[dead]', 'B': 'Madrid', 'C': '[dead]', 'D': 'Paris'}
        w = StringIO()
        diplomacy_print(w,r)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
            
            
    def test_print_3(self):
        r = {'A': '[dead]', 'B': '[dead]', 'C': '[dead]', 'D': '[dead]'}
        w = StringIO()
        diplomacy_print(w,r)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")


# ----
# main
# ----


if __name__ == "__main__": 
    main()
    
    
""" #pragma: no cover
"""
